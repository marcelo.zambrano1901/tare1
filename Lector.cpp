#include <iostream>
#include <fstream>

using namespace std;

int main() {
	
	// Variables.
    string nombreArchivo = "direcciones_ip.txt"; // Archivo txt.
    ifstream archivo(nombreArchivo.c_str()); // Transformar el archivo para leerlo.
    string direccionIP; // L�nea del archivo txt que contiene una direcci�n IP
    string numeroPaquetes = "4 "; // Cuantos paquetes se enviar�n al hacer el ping.
    
    // Se comienza a leer el archivo txt.
    while (getline(archivo, direccionIP)) {
        
        // Se crea el comando "ping -c 4 192.168.1.4"
        string comando = "ping -c " + numeroPaquetes + direccionIP;
        
        cout << endl << "[Haciendo ping a: " << direccionIP << "]" << endl;
        
        // Se ejecuta el comando.
        system(comando.c_str());
    }
    
}
